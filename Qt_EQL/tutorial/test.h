#ifndef APP_H
#define APP_H

#include <QObject>
#include <ecl/ecl.h>

class Test : public QObject {
    Q_OBJECT
public:
    Test(QObject* = nullptr, const QString& = QString());

    // define function acting as constructor (callable from Lisp)
    // N.B. return a vanilla Qt class (here: QObject*) known to EQL5
    Q_INVOKABLE QObject* newInstance(QObject* = nullptr, const QString& = QString());

public Q_SLOTS:
    // you may pass any type found in '~/eql5/src/ecl_fun.cpp::toMetaArg()';
    // it's common practice in Qt to always pass const references (except for
    // pointers, of course), as you'll find in any Qt example code
    QString concat(const QStringList&);

    // pass Lisp data ('cl_object' is just a pointer)
    void processData(cl_object);

    // call back to Lisp
    void printMe();
};

class Test2 : public Test { // inherits class above
    Q_OBJECT
public:
    Test2(QObject* = nullptr, const QString& = QString());

    // (see comment above)
    Q_INVOKABLE QObject* newInstance(QObject* = nullptr, const QString& = QString());

public Q_SLOTS:
    // (see comment above)
    void printAllMemberFunctions();
};

#endif
