QT           += svg printsupport uitools
TEMPLATE     = lib
CONFIG       += no_keywords release $$(SAILFISH)
CONFIG       += plugin
INCLUDEPATH  += ../src /usr/local/include
LIBS         += -lecl -leql5 -L.. -L/usr/local/lib
TARGET       = eql5_svg
DESTDIR      = ../
OBJECTS_DIR  = ./tmp/svg/
MOC_DIR      = ./tmp/svg/
VERSION      = $$(EQL_VERSION)

unix {
    target.path = $$[QT_INSTALL_LIBS]
}

osx {
    target.path = /usr/local/lib
}

INSTALLS = target

win32 {
    include(windows.pri)
}

sailfish {
    # on Sailfish run this prior to run qmake:
    # $ export SAILFISH=sailfish
    QT      -= printsupport uitools
    CONFIG  += shared_and_static build_all
    DEFINES += OS_SAILFISH
    message("*** Building for SailfishOS ***")
}

static {
    DESTDIR     = ./
    OBJECTS_DIR = ./tmp/static/svg/
    MOC_DIR     = ./tmp/static/svg/
    INSTALLS    -= target
}

HEADERS += gen/svg/_ini.h \
           gen/svg/_ini2.h \
           gen/svg/_q_classes.h \
           gen/svg/_n_classes.h \
           gen/svg/_q_methods.h \
           gen/svg/_n_methods.h

SOURCES += gen/svg/_ini.cpp
